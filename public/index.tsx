import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Main app component
import App from '../src/App-component';

ReactDOM.render(<App compiler="TypeScript" framework="React" />, document.getElementById('root'));