import * as React from 'react';

// Interface types
import { AppTypeI } from '../interface/AppTypeI.interface';

import './App-component.scss';

const App = (props: AppTypeI) => (
    <div className="Container">
        <h1>App is using: {props.framework}</h1>
        <h1>Compiles to: {props.compiler}</h1>
    </div>
);

export default App;